package nearby.habibqureshi.nearby.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.zip.Inflater;

import nearby.habibqureshi.nearby.Interfaces.OnClickInterface;
import nearby.habibqureshi.nearby.R;
import nearby.habibqureshi.nearby.Utils.M;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;
import static java.security.AccessController.getContext;

/**
 * Created by HabibQureshi on 10/17/2017.
 */

public class drawerListAdapter extends RecyclerView.Adapter<drawerListAdapter.ViewHolder> {
    View layout;
    LayoutInflater inflater;
    Bitmap icon;
    String []items;
    Context context;
    int Total;
    int icons[];


    public drawerListAdapter(int []icons,String[] items) {
        this.items = items;
        this.icons=icons;
        //this.icon=bitmap;
        M.l("drawerCons");

        this.Total=items.length;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            this.inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        layout=inflater.inflate(R.layout.drawer_adaper_layout,parent,false);
        M.l("createView");
        ViewHolder holder=new ViewHolder(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv.setText(this.items[position]);
        holder.iv.setImageResource(this.icons[position]);


    }

    @Override
    public int getItemCount() {
        return Total;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv;
        TextView tv;


        public ViewHolder(View itemView) {
            super(itemView);
            iv= (ImageView) itemView.findViewById(R.id.iv_NavIcon);
            tv= (TextView) itemView.findViewById(R.id.tv_NavTitle);

        }


    }
}
