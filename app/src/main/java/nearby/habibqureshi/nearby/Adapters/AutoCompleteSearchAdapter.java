package nearby.habibqureshi.nearby.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nearby.habibqureshi.nearby.AutoCompletePlacesClasses.Result;
import nearby.habibqureshi.nearby.R;

import static android.R.attr.data;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

/**
 * Created by HabibQureshi on 10/27/2017.
 */

public class AutoCompleteSearchAdapter extends RecyclerView.Adapter<AutoCompleteSearchAdapter.ViewHolder> {
    List<Result> list;
    LayoutInflater inflater;
    public AutoCompleteSearchAdapter(List<Result> list) {
        this.list=list;
    }

    @Override
    public AutoCompleteSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return new ViewHolder(inflater.inflate(R.layout.search_adaper_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(AutoCompleteSearchAdapter.ViewHolder holder, int position) {
        holder.tv.setText(list.get(position).getName());

    }

    @Override
    public int getItemCount() {
        if(list!=null)
        return list.size();
        else return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv;
        TextView tv;
        public ViewHolder(View itemView) {
            super(itemView);
            iv= (ImageView) itemView.findViewById(R.id.iv_NavIcon);
            tv= (TextView) itemView.findViewById(R.id.tv_NavTitle);
        }
    }
}
