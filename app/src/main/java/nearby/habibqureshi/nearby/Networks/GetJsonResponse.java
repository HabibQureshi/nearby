package nearby.habibqureshi.nearby.Networks;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import nearby.habibqureshi.nearby.Interfaces.OnJsonResponse;
import nearby.habibqureshi.nearby.Utils.M;

/**
 * Created by HabibQureshi on 10/18/2017.
 */

public class GetJsonResponse extends AsyncTask<String, Void, String> {
    OnJsonResponse jsonResponse;
    int RequestType;

    public GetJsonResponse(OnJsonResponse jsonResponse,int RequestType) {
        this.jsonResponse = jsonResponse;
        this.RequestType=RequestType;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        this.jsonResponse.onJsonResponse(response,RequestType);
    }

    @Override
    protected String doInBackground(String... params) {
        URL ulrn = null;
        try {
            ulrn = new URL(params[0]);
            M.l("doInBackground:",ulrn.toString());
            HttpURLConnection con = (HttpURLConnection) ulrn.openConnection();
            InputStream is = con.getInputStream();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(is));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            con.disconnect();
            return response.toString();


        } catch (Exception e) {
            M.l(e.getMessage() + " exception");
        }

        return null;
    }
}