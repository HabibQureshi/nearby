package nearby.habibqureshi.nearby.Utils;


/**
 * Created by HabibQureshi on 10/18/2017.
 */

public class Utils {

    public static String getNearByRequestUrl(double longitude, double latitude, String nearbyPlace, int PROXIMITY_RADIUS) {
        StringBuilder googlePlacesUrl = new StringBuilder(Constant.GooglePlaceApiBaseUrl);
        googlePlacesUrl.append(Constant.NearBySearch);
        googlePlacesUrl.append(Constant.JsonType);
        googlePlacesUrl.append(Constant.Location + longitude + "," + latitude);
        googlePlacesUrl.append(Constant.Radious + PROXIMITY_RADIUS);
        googlePlacesUrl.append(Constant.Type + nearbyPlace);
        googlePlacesUrl.append(Constant.Senser+"true");
        googlePlacesUrl.append(Constant.Key+ Constant.ApiKey);
        M.l(googlePlacesUrl.toString());
        return (googlePlacesUrl.toString());
        //https://maps.googleapis.com/maps/api/place/textsearch/json?query=ucp&key=AIzaSyBdw1tqyPIS8csvk4qKMPynUc0EY3k9a_0
    }
    public static String getNearByPlacesNextPageUrl(double longitude, double latitude, String nearbyPlace, int PROXIMITY_RADIUS,String NextPageToken){
        String url=getNearByRequestUrl(longitude,latitude,nearbyPlace,PROXIMITY_RADIUS);
        url=url+Constant.NextPageToken+NextPageToken;
        M.l(url);
        return url;

    }
    public static String getSuggestionPlaceRequestUrl(String Query) {
        StringBuilder googlePlacesUrl = new StringBuilder(Constant.GooglePlaceApiBaseUrl);
        googlePlacesUrl.append(Constant.TextSearch);
        googlePlacesUrl.append(Constant.JsonType);
        googlePlacesUrl.append(Constant.Query);
        googlePlacesUrl.append(Query);
        googlePlacesUrl.append(Constant.Key+ Constant.ApiKey);
        M.l(googlePlacesUrl.toString());
        return (googlePlacesUrl.toString());
        //https://maps.googleapis.com/maps/api/place/textsearch/json?query=ucp&key=AIzaSyBdw1tqyPIS8csvk4qKMPynUc0EY3k9a_0
    }
    public static String getPlaceDetailRequestUrl(String Query) {
        StringBuilder googlePlacesUrl = new StringBuilder(Constant.GooglePlaceApiBaseUrl);
        googlePlacesUrl.append(Constant.PlaceDetail);
        googlePlacesUrl.append(Constant.JsonType);
        googlePlacesUrl.append(Constant.PlaceID);
        googlePlacesUrl.append(Query);
        googlePlacesUrl.append(Constant.Key+ Constant.ApiKey);
        M.l(googlePlacesUrl.toString());
        return (googlePlacesUrl.toString());
        //https://maps.googleapis.com/maps/api/place/textsearch/json?query=ucp&key=AIzaSyBdw1tqyPIS8csvk4qKMPynUc0EY3k9a_0
    }
    public static String getDirectionUrl( double latitude,double longitude){
        StringBuilder googlePlacesUrl = new StringBuilder(Constant.GoogleDirection);
        googlePlacesUrl.append(Constant.JsonType);
        googlePlacesUrl.append(Constant.Origin);
        googlePlacesUrl.append(latitude+","+longitude);
        googlePlacesUrl.append(Constant.destination);
        googlePlacesUrl.append(UserLocation.getUserActualLocation().getLatitude()+","+UserLocation.getUserActualLocation().getLongitude());
        googlePlacesUrl.append(Constant.Key+ Constant.ApiKey);
        M.l(googlePlacesUrl.toString());
        return (googlePlacesUrl.toString());


    }

}
