package nearby.habibqureshi.nearby.Utils;

import android.location.Location;

/**
 * Created by HabibQureshi on 12/31/2017.
 */

public class UserLocation {
    static Location mLastLocation,userActualLocation;
    public static void setUserActualLocation(){
        if(userActualLocation==null)
            userActualLocation=new Location("Actual Location");
        userActualLocation.setLatitude(mLastLocation.getLatitude());
        userActualLocation.setLongitude(mLastLocation.getLongitude());
        M.l("userActualLocation",userActualLocation.getLatitude()+","+userActualLocation.getLongitude());

    }
    public static Location getUserActualLocation(){
        return userActualLocation;
    }
    public static void setLastLocation(Location location){
        mLastLocation=location;

    }
    public static void setLastLocation(double mLat, double mLongi){
        if(mLastLocation==null) {
            mLastLocation = new Location("User Current Location");
        }
        mLastLocation.setLongitude(mLongi);
        mLastLocation.setLatitude(mLat);
        M.l("setLastLocation",mLastLocation.getLatitude()+","+mLastLocation.getLongitude());

    }
    public static Location getLastLocation(){
        return mLastLocation;

    }

    public static double getLatitude() {
        return  mLastLocation.getLatitude();
    }
    public static double getLongitude() {
        return  mLastLocation.getLongitude();
    }

}
