package nearby.habibqureshi.nearby.Utils;

/**
 * Created by HabibQureshi on 10/26/2017.
 */

public class Constant{

    public static final int NEAR_PLACES_REQUEST=1;
    public static final int SUGGESTED_PLACES_REQUEST=2;
    public static final int PLACE_DETAIL=3;
    public static final int DIRECTION_REQUEST=4;
    public static final int Aproximity_Radious=1000;
    public static final String GoogleApiBase="https://maps.googleapis.com/maps/api/";
    public static final String GoogleDirection=GoogleApiBase+"directions/";
    public static final String GooglePlaceApiBaseUrl=GoogleApiBase+"place/";
    public static final String NearBySearch="nearbysearch/";
    public static final String PlaceDetail="details/";
    public static final String PlaceID="placeid=";
    public static final String TextSearch="textsearch/";
    public static final String JsonType="json?";
    public static final String XmlType="xml?";
    public static final String Query="query=";
    public static final String Key="&key=";
    public static final String Location= "location=";
    public static final String Radious= "&radius=";
    public static final String Type= "&type=";
    public static final String Senser= "&sensor=";
    public static final String ApiKey="AIzaSyBdw1tqyPIS8csvk4qKMPynUc0EY3k9a_0";
    public static final String NextPageToken="&pagetoken=";
    public static final String Origin="origin=";
    public static final String destination="&destination=";













}
