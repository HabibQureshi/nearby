package nearby.habibqureshi.nearby.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import java.util.List;

import nearby.habibqureshi.nearby.Adapters.AutoCompleteSearchAdapter;
import nearby.habibqureshi.nearby.AutoCompletePlacesClasses.Result;
import nearby.habibqureshi.nearby.AutoCompletePlacesClasses.SuggestedPlaces;
import nearby.habibqureshi.nearby.Directions.Direction;
import nearby.habibqureshi.nearby.Fragments.MainMap;
import nearby.habibqureshi.nearby.Fragments.Navigation;
import nearby.habibqureshi.nearby.Interfaces.InformMe;
import nearby.habibqureshi.nearby.Interfaces.OnClickInterface;
import nearby.habibqureshi.nearby.Interfaces.OnJsonResponse;
import nearby.habibqureshi.nearby.Interfaces.getLocationCheck;
import nearby.habibqureshi.nearby.Networks.GetJsonResponse;
import nearby.habibqureshi.nearby.PlaceDetailResponse.PlaceDetail;
import nearby.habibqureshi.nearby.R;
import nearby.habibqureshi.nearby.Utils.Constant;
import nearby.habibqureshi.nearby.Utils.M;
import nearby.habibqureshi.nearby.Utils.UserLocation;
import nearby.habibqureshi.nearby.Utils.Utils;



public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,SearchView.OnCloseListener,SearchView.OnQueryTextListener,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, OnClickInterface, OnJsonResponse,OnCompleteListener<Location> {
    private InformMe inform;
    private static final String TAG = MainActivity.class.getSimpleName();
    Toolbar toolbar;
    private BottomSheetBehavior bottomSheetBehavior;
    RecyclerView searchSugesstion;
    Navigation navigation;
    android.support.v4.app.FragmentManager fragmentManager;
    LocationManager manager;
    SearchView searchView;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private static int UPDATE_INTERVAL = 10000;
    private static int FASTEST_INTERVAL = 5000;
    private static int DISPLACEMENT = 10;
    getLocationCheck check = null;
    public static boolean isConnected = false;
    Task<Location> mLastLocation;
    List<Result> PlaceSuggestion;
    private ProgressDialog progress;
    FusedLocationProviderClient fusedLocationProviderClient;
    private SuggestedPlaces suggestedPlaces;
    private PlaceDetail placeDetail;
    private Direction direction;
    private TextView PlaceName;
    private TextView PlaceAddres;
    private TextView PlaceRating;
    private TextView PlacePhone;
    private TextView PlaceWeb;
    private TextView PlaceTimings;
    private Button getDirection;
    Fragment fragment;
    String searchPlace = null;
    int PlaceIcon=0;
    private AsyncTask<String, Void, String> task;
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        M.l("MainActivity onCreate");
        initFindViewsById();
        init();
        if (!check_network())
            M.t(this, getString(R.string.enableInternet));
        if(savedInstanceState!=null)
        {
            fragment = getSupportFragmentManager().getFragment(savedInstanceState, "currentFragment");
        }
        else {
            fragment=new MainMap();
        }
        this.changeFragment(fragment);
    }

    @Override
    protected void onResume() {
        M.l("MainActivity onResume");

        super.onResume();

    }


    public void init() {
        M.l("initMainActivity");
        this.buildGoogleApiClient();
        this.createLocationRequest();
        fragmentManager = getSupportFragmentManager();
        searchSugesstion= findViewById(R.id.search_suggestion);
        searchSugesstion.setAdapter(new AutoCompleteSearchAdapter(PlaceSuggestion));
        searchSugesstion.setLayoutManager(new LinearLayoutManager(this));
        searchSugesstion.addOnItemTouchListener(new Navigation.RecyclerTouchListener(this, searchSugesstion, new OnClickInterface() {
            @Override
            public void drawerOnClick(View view, int position) {
                CloseSearchSuggestion();
                M.l("drawerOnClick");
              /*  TextView tv= (TextView) view.findViewById(R.id.tv_NavTitle);
                M.t(MainActivity.this,tv.getText().toString());*/
              if(suggestedPlaces.getResults().get(0).getGeometry().getLocation().getLat()==null)
                  M.l("PlaceSuggestion is null");
              else M.l("not null");
              M.l(position+"");
                if (inform!=null)
                        inform.setPlaceOnMap(suggestedPlaces.getResults().get(position).getGeometry().getLocation().getLat(),suggestedPlaces.getResults().get(position).getGeometry().getLocation().getLng(),suggestedPlaces.getResults().get(position).getName(),suggestedPlaces.getResults().get(position).getPlaceId());

            }
        }));
        manager = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        setSupportActionBar(toolbar);
        this.searchView=  toolbar.findViewById(R.id.search_tool_bar);
        this.searchView.setOnCloseListener(this);
        this.searchView.setOnQueryTextListener(this);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setTitle("NearBy");
        navigation.setUp(R.id.nav, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar, this, this);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        this.getDirection.setOnClickListener(clickListener);



    }

    public boolean isGoogleApiClientConnected() {
        return this.mGoogleApiClient.isConnected();
    }


    public void initFindViewsById() {
        bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.Bottom_sheet));
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        PlaceName =  findViewById(R.id.place_name);
        PlaceAddres =  findViewById(R.id.place_address);
        PlacePhone =  findViewById(R.id.place_Phone);
        PlaceRating =  findViewById(R.id.place_rating);
        PlaceTimings =  findViewById(R.id.place_timing);
        PlaceWeb =  findViewById(R.id.Place_web);
        toolbar =  findViewById(R.id.include_toolbar);
        getDirection =findViewById(R.id.getDirection);
        navigation = (Navigation) getSupportFragmentManager().findFragmentById(R.id.nav);

    }


    void changeFragment(Fragment fragment) {
        fragmentManager.beginTransaction()
                .replace(R.id.main_content, fragment)
                .commit();
    }

    public boolean isGPSEnable() {
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public void displayLocationSettingsRequest() {
        M.l("request for gps");
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                M.l("Location setting result");
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        if (isConnected)
                            if (check != null)
                                check.canGetLocation(true);
                        Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(MainActivity.this, 100);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    public boolean checkLocationAppPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    public void requestLocationAppPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                99);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 99: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        M.l("permission granted");

                    }

                } else {
                    M.t(this, "Permission Require");
                    this.requestLocationAppPermission();
                }
                return;
            }
        }
    }

    @Override
    protected void onStart() {
            super.onStart();
        isConnected=true;
        M.l("MainActivity onStart");
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();

    }

    @Override
    protected void onStop() {
        super.onStop();
        M.l("MainActivity onStop");

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        M.l("MainActivity onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        M.l("MainActivity onDestroy");
    }

    public boolean check_network() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        isConnected = true;
        M.l("On Connected");


    }

    @Override
    public void onConnectionSuspended(int i) {
        isConnected = false;
        if (check != null)
            check.canGetLocation(isConnected);

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        isConnected = false;
        if (check != null)
            check.canGetLocation(isConnected);


    }

    @Override
    public void onLocationChanged(Location location) {
        M.l("Location = " + location.getLatitude() + " " + location.getLongitude());


    }

    public void getLocation(OnCompleteListener<Location> listener) {
        if (this.checkLocationAppPermission()) {
            if (this.isGPSEnable()) {
                if (isConnected) {
                    mLastLocation = fusedLocationProviderClient.getLastLocation();
                    mLastLocation.addOnCompleteListener(listener);
                }
            } else
                displayLocationSettingsRequest();
        } else this.requestLocationAppPermission();

    }

    public void requestForLocation(getLocationCheck check) {
        this.check = check;
    }

    @Override
    public void drawerOnClick(View view, int position) {
        M.l(position + "");

        switch (position) {
            case 0:
                searchPlace = "restaurant";
                PlaceIcon=R.drawable.thumb_hotel;
                break;
            case 1:
                searchPlace = "casino";
                PlaceIcon=R.drawable.thumb_casino;
                break;
            case 2:
                searchPlace = "shopping_mall";
                PlaceIcon=R.drawable.thumb_shopping;
                break;
            case 3:
                searchPlace = "park";
                PlaceIcon=R.drawable.thumb_park;
                break;
            case 4:
                searchPlace = "shopping_mall";
                PlaceIcon=R.drawable.thumb_shopping;
                break;
            case 5:
                searchPlace = "restaurant";
                PlaceIcon=R.drawable.thumb_hotel;
                break;
            case 6:
                searchPlace = "hospital";
                PlaceIcon=R.drawable.thumb_dod;
                break;

        }
        this.navigation.closeDrawer();
        if (UserLocation.getLastLocation() != null) {
            getPlaces();
        } else M.t(this, "You'r location is unknown");

    }

    private void getPlaces() {
        if (check_network()) {

            if((task==null)||(task!=null&&task.getStatus()==AsyncTask.Status.FINISHED))
                {
                    M.l("request","req ja rhia ha bhi");
                    task=new GetJsonResponse(this, Constant.NEAR_PLACES_REQUEST).execute( Utils.getNearByRequestUrl(UserLocation.getLatitude(), UserLocation.getLongitude(), searchPlace, Constant.Aproximity_Radious));

                }
            else M.l("request","cancel request, request is already in progress");

        } else M.t(this, getString(R.string.enableInternet));
    }
    private void getNextPagePlaces(String token){
        if (check_network()) {
                task=new GetJsonResponse(this, Constant.NEAR_PLACES_REQUEST).execute( Utils.getNearByPlacesNextPageUrl(UserLocation.getLatitude(), UserLocation.getLongitude(), searchPlace, Constant.Aproximity_Radious,token));
        } else M.t(this, getString(R.string.enableInternet));
    }

    public void informMeAboutJsonResponse(InformMe mainMap) {
        this.inform = mainMap;
    }

    public void SuggestionPlacesRequest(String Query) {
        new GetJsonResponse(this,Constant.SUGGESTED_PLACES_REQUEST).execute(Utils.getSuggestionPlaceRequestUrl(Query));
    }


    @Override
    public void onJsonResponse(String response,int ResponseType) {
        cancleProgressMessage();
        switch (ResponseType){
            case Constant.NEAR_PLACES_REQUEST:
                if (this.inform != null)
                    inform.PlacesResponse(response);
                break;
            case Constant.SUGGESTED_PLACES_REQUEST:
                    if (response != null) {
                        Gson gson = new Gson();
                        suggestedPlaces = gson.fromJson(response, SuggestedPlaces.class);
                        M.l("response",response.toString());
                        PlaceSuggestion=suggestedPlaces.getResults();

                        M.l("suggested status:"+suggestedPlaces.getStatus());
                        M.l("suggested result:"+suggestedPlaces.getResults().toString());

                        if (suggestedPlaces.getStatus().equals("OK")) {
                            searchSugesstion.swapAdapter(new AutoCompleteSearchAdapter(suggestedPlaces.getResults()),true);

                        } else {
                            M.l(suggestedPlaces.getStatus());
                        }
                    }

                break;
            case Constant.PLACE_DETAIL:
                if(response!=null)
                {
                    M.l(response);
                    Gson gson = new Gson();
                    placeDetail=gson.fromJson(response,PlaceDetail.class);
                    if(placeDetail.getStatus().equals("OK"))
                        this.SetPlaceDetails();
                    else M.t(getApplicationContext(),"PlaceDetails return null");
                }


                break;
            case Constant.DIRECTION_REQUEST:
                if(response!=null)
                {
                    M.l(response);
                    Gson gson = new Gson();
                    direction=gson.fromJson(response,Direction.class);
                    if(direction.getStatus().equals("OK"))
                        this.inform.DrawDirection(direction);
                    else M.t(getApplicationContext(),"PlaceDetails return null");
                }

                break;

        }

    }

    public void showProgressMessage(String message) {
        M.l("showing");
        this.inform.showIndicator();

//        progress = new ProgressDialog(this);
//        progress.setMessage(message);
//        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progress.setIndeterminate(false);
//        progress.setCanceledOnTouchOutside(false);
//        progress.show();
    }

    public Boolean isProgressShowing() {
        if (this.progress != null)
            if (this.progress.isShowing())
                return true;
        return false;
    }

    public void updateProgressMessage(final String s) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progress.setMessage(s);
            }
        });
    }

    public void cancleProgressMessage() {
        M.l("hiding");
        this.inform.hideIndicator();
//        if ( loadingIndicatorView != null)
//            if ( loadingIndicatorView.isShown())
//                this.loadingIndicatorView.hide();
    }

    @Override
    public void onComplete(@NonNull Task<Location> task) {
        if(task.isSuccessful())
            task.getResult();
    }
    public void CloseSearchSuggestion(){
        PlaceSuggestion=null;
        searchSugesstion.swapAdapter(new AutoCompleteSearchAdapter(PlaceSuggestion),false);
    }

    @Override
    public boolean onClose() {
      CloseSearchSuggestion();
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        SuggestionPlacesRequest(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        //SuggestionPlacesRequest(newText);
        return false;
    }
    public void getPlaceDetail(String URL,int RequestType){
        this.showProgressMessage("Getting Details");
        new GetJsonResponse(this,RequestType).execute(URL);

    }
    void SetPlaceDetails(){
        try {
            PlaceName.setText(this.placeDetail.getResult().getName());
            PlaceAddres.setText(this.placeDetail.getResult().getVicinity());
            PlacePhone.setText(this.placeDetail.getResult().getFormattedPhoneNumber());
            PlaceRating.setText(this.placeDetail.getResult().getRating().toString());
            String Timing = "";
            for (int i = 0; i < this.placeDetail.getResult().getOpeningHours().getWeekdayText().size(); i++)
                Timing += this.placeDetail.getResult().getOpeningHours().getWeekdayText().get(i) + "\n";

            PlaceTimings.setText(Timing);
            PlaceWeb.setText(this.placeDetail.getResult().getWebsite());

        }
        catch (NullPointerException e){
            M.l(e.getMessage().toString());
        }
        if(navigation.isDrawerOpen())
            navigation.closeDrawer();
        this.bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

    }

    @Override
    public void onBackPressed() {
        if(this.bottomSheetBehavior.getState()==BottomSheetBehavior.STATE_EXPANDED)
            this.bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        else
        if(navigation.isDrawerOpen())
            navigation.closeDrawer();
        else
        super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, "currentFragment", fragment);
    }
    public void getPlaces(boolean getplaces){
        if(searchPlace!=null&&getplaces)
            getPlaces();

    }
    public void NextPagePlaces(String Token){
       this.getNextPagePlaces(Token);

    }
    View.OnClickListener clickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.getDirection:
                    GetDirection();
                    break;
            }


        }
    };
    public void GetDirection(){
        if(placeDetail!=null){
            new GetJsonResponse(this,Constant.DIRECTION_REQUEST).execute(Utils.getDirectionUrl(placeDetail.getResult().getGeometry().getLocation().getLat(),placeDetail.getResult().getGeometry().getLocation().getLng()));

        }

    }
    public int getIconforMap(){
        return PlaceIcon;

    }

}


