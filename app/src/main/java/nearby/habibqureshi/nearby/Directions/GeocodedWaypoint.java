
package nearby.habibqureshi.nearby.Directions;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class GeocodedWaypoint {

    @SerializedName("geocoder_status")
    private String mGeocoderStatus;
    @SerializedName("place_id")
    private String mPlaceId;
    @SerializedName("types")
    private List<String> mTypes;

    public String getGeocoderStatus() {
        return mGeocoderStatus;
    }

    public String getPlaceId() {
        return mPlaceId;
    }

    public List<String> getTypes() {
        return mTypes;
    }

    public static class Builder {

        private String mGeocoderStatus;
        private String mPlaceId;
        private List<String> mTypes;

        public GeocodedWaypoint.Builder withGeocoderStatus(String geocoderStatus) {
            mGeocoderStatus = geocoderStatus;
            return this;
        }

        public GeocodedWaypoint.Builder withPlaceId(String placeId) {
            mPlaceId = placeId;
            return this;
        }

        public GeocodedWaypoint.Builder withTypes(List<String> types) {
            mTypes = types;
            return this;
        }

        public GeocodedWaypoint build() {
            GeocodedWaypoint GeocodedWaypoint = new GeocodedWaypoint();
            GeocodedWaypoint.mGeocoderStatus = mGeocoderStatus;
            GeocodedWaypoint.mPlaceId = mPlaceId;
            GeocodedWaypoint.mTypes = mTypes;
            return GeocodedWaypoint;
        }

    }

}
