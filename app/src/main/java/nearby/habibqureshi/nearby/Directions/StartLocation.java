
package nearby.habibqureshi.nearby.Directions;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class StartLocation {

    @SerializedName("lat")
    private Double mLat;
    @SerializedName("lng")
    private Double mLng;

    public Double getLat() {
        return mLat;
    }

    public Double getLng() {
        return mLng;
    }

    public static class Builder {

        private Double mLat;
        private Double mLng;

        public StartLocation.Builder withLat(Double lat) {
            mLat = lat;
            return this;
        }

        public StartLocation.Builder withLng(Double lng) {
            mLng = lng;
            return this;
        }

        public StartLocation build() {
            StartLocation StartLocation = new StartLocation();
            StartLocation.mLat = mLat;
            StartLocation.mLng = mLng;
            return StartLocation;
        }

    }

}
