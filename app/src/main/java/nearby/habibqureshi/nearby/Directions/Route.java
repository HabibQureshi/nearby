
package nearby.habibqureshi.nearby.Directions;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Route {

    @SerializedName("bounds")
    private Bounds mBounds;
    @SerializedName("copyrights")
    private String mCopyrights;
    @SerializedName("legs")
    private List<Leg> mLegs;
    @SerializedName("overview_polyline")
    private OverviewPolyline mOverviewPolyline;
    @SerializedName("summary")
    private String mSummary;
    @SerializedName("warnings")
    private List<Object> mWarnings;
    @SerializedName("waypoint_order")
    private List<Object> mWaypointOrder;

    public Bounds getBounds() {
        return mBounds;
    }

    public String getCopyrights() {
        return mCopyrights;
    }

    public List<Leg> getLegs() {
        return mLegs;
    }

    public OverviewPolyline getOverviewPolyline() {
        return mOverviewPolyline;
    }

    public String getSummary() {
        return mSummary;
    }

    public List<Object> getWarnings() {
        return mWarnings;
    }

    public List<Object> getWaypointOrder() {
        return mWaypointOrder;
    }

    public static class Builder {

        private Bounds mBounds;
        private String mCopyrights;
        private List<Leg> mLegs;
        private OverviewPolyline mOverviewPolyline;
        private String mSummary;
        private List<Object> mWarnings;
        private List<Object> mWaypointOrder;

        public Route.Builder withBounds(Bounds bounds) {
            mBounds = bounds;
            return this;
        }

        public Route.Builder withCopyrights(String copyrights) {
            mCopyrights = copyrights;
            return this;
        }

        public Route.Builder withLegs(List<Leg> legs) {
            mLegs = legs;
            return this;
        }

        public Route.Builder withOverviewPolyline(OverviewPolyline overviewPolyline) {
            mOverviewPolyline = overviewPolyline;
            return this;
        }

        public Route.Builder withSummary(String summary) {
            mSummary = summary;
            return this;
        }

        public Route.Builder withWarnings(List<Object> warnings) {
            mWarnings = warnings;
            return this;
        }

        public Route.Builder withWaypointOrder(List<Object> waypointOrder) {
            mWaypointOrder = waypointOrder;
            return this;
        }

        public Route build() {
            Route Route = new Route();
            Route.mBounds = mBounds;
            Route.mCopyrights = mCopyrights;
            Route.mLegs = mLegs;
            Route.mOverviewPolyline = mOverviewPolyline;
            Route.mSummary = mSummary;
            Route.mWarnings = mWarnings;
            Route.mWaypointOrder = mWaypointOrder;
            return Route;
        }

    }

}
