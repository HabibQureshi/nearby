
package nearby.habibqureshi.nearby.Directions;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Polyline {

    @SerializedName("points")
    private String mPoints;

    public String getPoints() {
        return mPoints;
    }

    public static class Builder {

        private String mPoints;

        public Polyline.Builder withPoints(String points) {
            mPoints = points;
            return this;
        }

        public Polyline build() {
            Polyline Polyline = new Polyline();
            Polyline.mPoints = mPoints;
            return Polyline;
        }

    }

}
