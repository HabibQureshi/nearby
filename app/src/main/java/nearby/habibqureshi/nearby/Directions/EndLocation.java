
package nearby.habibqureshi.nearby.Directions;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class EndLocation {

    @SerializedName("lat")
    private Double mLat;
    @SerializedName("lng")
    private Double mLng;

    public Double getLat() {
        return mLat;
    }

    public Double getLng() {
        return mLng;
    }

    public static class Builder {

        private Double mLat;
        private Double mLng;

        public EndLocation.Builder withLat(Double lat) {
            mLat = lat;
            return this;
        }

        public EndLocation.Builder withLng(Double lng) {
            mLng = lng;
            return this;
        }

        public EndLocation build() {
            EndLocation EndLocation = new EndLocation();
            EndLocation.mLat = mLat;
            EndLocation.mLng = mLng;
            return EndLocation;
        }

    }

}
