
package nearby.habibqureshi.nearby.Directions;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Leg {

    @SerializedName("distance")
    private Distance mDistance;
    @SerializedName("duration")
    private Duration mDuration;
    @SerializedName("end_address")
    private String mEndAddress;
    @SerializedName("end_location")
    private EndLocation mEndLocation;
    @SerializedName("start_address")
    private String mStartAddress;
    @SerializedName("start_location")
    private StartLocation mStartLocation;
    @SerializedName("steps")
    private List<Step> mSteps;
    @SerializedName("traffic_speed_entry")
    private List<Object> mTrafficSpeedEntry;
    @SerializedName("via_waypoint")
    private List<Object> mViaWaypoint;

    public Distance getDistance() {
        return mDistance;
    }

    public Duration getDuration() {
        return mDuration;
    }

    public String getEndAddress() {
        return mEndAddress;
    }

    public EndLocation getEndLocation() {
        return mEndLocation;
    }

    public String getStartAddress() {
        return mStartAddress;
    }

    public StartLocation getStartLocation() {
        return mStartLocation;
    }

    public List<Step> getSteps() {
        return mSteps;
    }

    public List<Object> getTrafficSpeedEntry() {
        return mTrafficSpeedEntry;
    }

    public List<Object> getViaWaypoint() {
        return mViaWaypoint;
    }

    public static class Builder {

        private Distance mDistance;
        private Duration mDuration;
        private String mEndAddress;
        private EndLocation mEndLocation;
        private String mStartAddress;
        private StartLocation mStartLocation;
        private List<Step> mSteps;
        private List<Object> mTrafficSpeedEntry;
        private List<Object> mViaWaypoint;

        public Leg.Builder withDistance(Distance distance) {
            mDistance = distance;
            return this;
        }

        public Leg.Builder withDuration(Duration duration) {
            mDuration = duration;
            return this;
        }

        public Leg.Builder withEndAddress(String endAddress) {
            mEndAddress = endAddress;
            return this;
        }

        public Leg.Builder withEndLocation(EndLocation endLocation) {
            mEndLocation = endLocation;
            return this;
        }

        public Leg.Builder withStartAddress(String startAddress) {
            mStartAddress = startAddress;
            return this;
        }

        public Leg.Builder withStartLocation(StartLocation startLocation) {
            mStartLocation = startLocation;
            return this;
        }

        public Leg.Builder withSteps(List<Step> steps) {
            mSteps = steps;
            return this;
        }

        public Leg.Builder withTrafficSpeedEntry(List<Object> trafficSpeedEntry) {
            mTrafficSpeedEntry = trafficSpeedEntry;
            return this;
        }

        public Leg.Builder withViaWaypoint(List<Object> viaWaypoint) {
            mViaWaypoint = viaWaypoint;
            return this;
        }

        public Leg build() {
            Leg Leg = new Leg();
            Leg.mDistance = mDistance;
            Leg.mDuration = mDuration;
            Leg.mEndAddress = mEndAddress;
            Leg.mEndLocation = mEndLocation;
            Leg.mStartAddress = mStartAddress;
            Leg.mStartLocation = mStartLocation;
            Leg.mSteps = mSteps;
            Leg.mTrafficSpeedEntry = mTrafficSpeedEntry;
            Leg.mViaWaypoint = mViaWaypoint;
            return Leg;
        }

    }

}
