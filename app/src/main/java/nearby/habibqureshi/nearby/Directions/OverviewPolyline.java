
package nearby.habibqureshi.nearby.Directions;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OverviewPolyline {

    @SerializedName("points")
    private String mPoints;

    public String getPoints() {
        return mPoints;
    }

    public static class Builder {

        private String mPoints;

        public OverviewPolyline.Builder withPoints(String points) {
            mPoints = points;
            return this;
        }

        public OverviewPolyline build() {
            OverviewPolyline OverviewPolyline = new OverviewPolyline();
            OverviewPolyline.mPoints = mPoints;
            return OverviewPolyline;
        }

    }

}
