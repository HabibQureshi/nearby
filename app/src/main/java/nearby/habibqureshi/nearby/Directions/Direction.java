
package nearby.habibqureshi.nearby.Directions;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Direction {

    @SerializedName("geocoded_waypoints")
    private List<GeocodedWaypoint> mGeocodedWaypoints;
    @SerializedName("routes")
    private List<Route> mRoutes;
    @SerializedName("status")
    private String mStatus;

    public List<GeocodedWaypoint> getGeocodedWaypoints() {
        return mGeocodedWaypoints;
    }

    public List<Route> getRoutes() {
        return mRoutes;
    }

    public String getStatus() {
        return mStatus;
    }

    public static class Builder {

        private List<GeocodedWaypoint> mGeocodedWaypoints;
        private List<Route> mRoutes;
        private String mStatus;

        public Direction.Builder withGeocodedWaypoints(List<GeocodedWaypoint> geocodedWaypoints) {
            mGeocodedWaypoints = geocodedWaypoints;
            return this;
        }

        public Direction.Builder withRoutes(List<Route> routes) {
            mRoutes = routes;
            return this;
        }

        public Direction.Builder withStatus(String status) {
            mStatus = status;
            return this;
        }

        public Direction build() {
            Direction Direction = new Direction();
            Direction.mGeocodedWaypoints = mGeocodedWaypoints;
            Direction.mRoutes = mRoutes;
            Direction.mStatus = mStatus;
            return Direction;
        }

    }

}
