
package nearby.habibqureshi.nearby.Directions;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Step {

    @SerializedName("distance")
    private Distance mDistance;
    @SerializedName("duration")
    private Duration mDuration;
    @SerializedName("end_location")
    private EndLocation mEndLocation;
    @SerializedName("html_instructions")
    private String mHtmlInstructions;
    @SerializedName("maneuver")
    private String mManeuver;
    @SerializedName("polyline")
    private Polyline mPolyline;
    @SerializedName("start_location")
    private StartLocation mStartLocation;
    @SerializedName("travel_mode")
    private String mTravelMode;

    public Distance getDistance() {
        return mDistance;
    }

    public Duration getDuration() {
        return mDuration;
    }

    public EndLocation getEndLocation() {
        return mEndLocation;
    }

    public String getHtmlInstructions() {
        return mHtmlInstructions;
    }

    public String getManeuver() {
        return mManeuver;
    }

    public Polyline getPolyline() {
        return mPolyline;
    }

    public StartLocation getStartLocation() {
        return mStartLocation;
    }

    public String getTravelMode() {
        return mTravelMode;
    }

    public static class Builder {

        private Distance mDistance;
        private Duration mDuration;
        private EndLocation mEndLocation;
        private String mHtmlInstructions;
        private String mManeuver;
        private Polyline mPolyline;
        private StartLocation mStartLocation;
        private String mTravelMode;

        public Step.Builder withDistance(Distance distance) {
            mDistance = distance;
            return this;
        }

        public Step.Builder withDuration(Duration duration) {
            mDuration = duration;
            return this;
        }

        public Step.Builder withEndLocation(EndLocation endLocation) {
            mEndLocation = endLocation;
            return this;
        }

        public Step.Builder withHtmlInstructions(String htmlInstructions) {
            mHtmlInstructions = htmlInstructions;
            return this;
        }

        public Step.Builder withManeuver(String maneuver) {
            mManeuver = maneuver;
            return this;
        }

        public Step.Builder withPolyline(Polyline polyline) {
            mPolyline = polyline;
            return this;
        }

        public Step.Builder withStartLocation(StartLocation startLocation) {
            mStartLocation = startLocation;
            return this;
        }

        public Step.Builder withTravelMode(String travelMode) {
            mTravelMode = travelMode;
            return this;
        }

        public Step build() {
            Step Step = new Step();
            Step.mDistance = mDistance;
            Step.mDuration = mDuration;
            Step.mEndLocation = mEndLocation;
            Step.mHtmlInstructions = mHtmlInstructions;
            Step.mManeuver = mManeuver;
            Step.mPolyline = mPolyline;
            Step.mStartLocation = mStartLocation;
            Step.mTravelMode = mTravelMode;
            return Step;
        }

    }

}
