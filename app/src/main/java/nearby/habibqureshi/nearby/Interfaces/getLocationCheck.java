package nearby.habibqureshi.nearby.Interfaces;

import android.location.Location;

/**
 * Created by HabibQureshi on 10/19/2017.
 */

public interface getLocationCheck {
    void canGetLocation(Boolean canConnect);
    void getUpdatedLocation(Location location);
}
