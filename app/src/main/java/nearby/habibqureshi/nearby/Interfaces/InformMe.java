package nearby.habibqureshi.nearby.Interfaces;

import android.location.Location;

import nearby.habibqureshi.nearby.Directions.Direction;

/**
 * Created by HabibQureshi on 10/21/2017.
 */

public interface InformMe {
    void PlacesResponse(String Places);
    void setPlaceOnMap(Double lat, Double lng, String name, String PlaceID);
    void showIndicator();
    void hideIndicator();
    void DrawDirection(Direction direction);
}
