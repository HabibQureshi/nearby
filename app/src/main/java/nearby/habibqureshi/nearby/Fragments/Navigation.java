package nearby.habibqureshi.nearby.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import nearby.habibqureshi.nearby.Adapters.drawerListAdapter;
import nearby.habibqureshi.nearby.Interfaces.OnClickInterface;
import nearby.habibqureshi.nearby.Utils.M;

import nearby.habibqureshi.nearby.R;

/**
 *
 */
public class Navigation extends Fragment {
    public ActionBarDrawerToggle actionBarDrawerToggle;
    private DrawerLayout drawerLayout;
    private View containerView,layout;
    private RecyclerView recyclerView;
    drawerListAdapter adapter;
    OnClickInterface clickInterface;
    Context context;
    Toolbar toolbar;
    int icons[]={R.drawable.thumb_hotel,R.drawable.thumb_casino,R.drawable.thumb_famous,R.drawable.thumb_park,R.drawable.thumb_shopping,R.drawable.thumb_historical,R.drawable.thumb_dod};
    //String text[];


    public Navigation() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout= inflater.inflate(R.layout.fragment_navigation, container, false);
        recyclerView= (RecyclerView) layout.findViewById(R.id.drawerList);


        return layout;
    }

    public void setUp(int FragmentID, DrawerLayout drawerLayout, final Toolbar toolbar, Context c, final OnClickInterface onClickInterface) {
        M.l("setupNavigation");
        this.context=c;
        this.toolbar=toolbar;


        adapter=new drawerListAdapter(this.icons,getResources().getStringArray(R.array.category_items));
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        this.clickInterface=onClickInterface;

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(context, recyclerView, new OnClickInterface() {
            @Override
            public void drawerOnClick(View view, int position) {
                M.l("drawerOnClick");
                onClickInterface.drawerOnClick(view,position);

            }
        }));
        this.containerView=getActivity().findViewById(FragmentID);
        this.drawerLayout=drawerLayout;
        actionBarDrawerToggle= new ActionBarDrawerToggle(getActivity(),drawerLayout,this.toolbar,R.string.drawer_open,R.string.drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                actionBarDrawerToggle.syncState();
            }
        });

    }
        public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private OnClickInterface clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final OnClickInterface clickListener) {
            this.clickListener = clickListener;
            M.l("RecyclerTouchListener");
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    M.l("onSingleTapUp");
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    M.l("onLongPress");
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        //clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            M.l("onInterceptTouchEvent");

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.drawerOnClick(child, rv.getChildAdapterPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            M.l("onTouchEvent");
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            M.l("onRequestDisallowInterceptTouchEvent");

        }


    }
    public void openDrawer() {

        this.drawerLayout.openDrawer(containerView);
    }

    public void closeDrawer() {

        this.drawerLayout.closeDrawer(containerView);
    }

    public boolean isDrawerOpen() {

        return  this.drawerLayout != null &&  this.drawerLayout.isDrawerOpen(containerView);
    }
    public void showHamburgerIcon() {
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
    }

    public void showBackIcon() {

        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);

    }
}
