package nearby.habibqureshi.nearby.Fragments;


import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import nearby.habibqureshi.nearby.Directions.Direction;
import nearby.habibqureshi.nearby.Interfaces.InformMe;
import nearby.habibqureshi.nearby.Activities.MainActivity;
import nearby.habibqureshi.nearby.Utils.Constant;
import nearby.habibqureshi.nearby.Utils.M;
import nearby.habibqureshi.nearby.NearByJsonClasses.Result;
import nearby.habibqureshi.nearby.NearByJsonClasses.nearby;
import nearby.habibqureshi.nearby.R;
import nearby.habibqureshi.nearby.Utils.Utils;
import nearby.habibqureshi.nearby.Utils.UserLocation;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainMap extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerDragListener,
        InformMe,
        OnCompleteListener<Location>,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraMoveCanceledListener,
        GoogleMap.OnCameraIdleListener {
    Location current, newCurrent;
    AVLoadingIndicatorView loadingIndicatorView;
    GoogleMap map;
    Context context;
    View layout;
    private nearby nb;
    MainActivity mainActivity;
    ImageButton currentLocation;
    public static final String TAG = MainMap.class.getSimpleName();
    SupportMapFragment mapFragment;
    Circle circle;
    private boolean getPlcaes = false, placesOnMap = false, NextPageToken = false, directionset = false;
    int distance;


    @Override
    public void onAttach(Context context) {
        M.l("Fragment onAttach");
        this.context = context;
        mainActivity = (MainActivity) context;
        if (!mainActivity.check_network())
            M.t(context, getString(R.string.enableInternet));
        super.onAttach(context);
    }

    public CameraPosition currentCameraPosition;

    public MainMap() {


    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        M.l("Fragment OnCreate");
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        M.l("Fragment onActivityCreated");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        M.l("Fragment onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
       /* if(nearbyPlaces !=null)
            DisplayNearBy(nearbyPlaces);
*/
        M.l("Fragment onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        M.l("Fragment onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        M.l("Fragment onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        M.l("Fragment onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        M.l("Fragment onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        M.l("Fragment onDetach");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        M.l("Fragment onCreateView");
        layout = inflater.inflate(R.layout.fragment_main_map, container, false);
        current = new Location("current");
        newCurrent = new Location("new Current");
        setupMapIfNeeded();
        currentLocation = layout.findViewById(R.id.current_location);
        loadingIndicatorView = layout.findViewById(R.id.avi);
        currentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLastLocation();
            }
        });
        ((MainActivity) getActivity()).informMeAboutJsonResponse(this);
        return layout;
    }

    private void setupMapIfNeeded() {
        if (map == null) {
            mapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
            mapFragment.setRetainInstance(true);
        } else {
            setUpMap();
            M.l("Fragment map was not null");
        }

    }

    void setUp1KmRadius() {
        circle = this.map.addCircle(new CircleOptions()
                .radius(1000)
                .center(new LatLng(0.0, 0.0))
                .strokeColor(getResources().getColor(R.color.gray))
                .fillColor(Color.argb(100, 245, 245, 245)));

    }

    void currentPositionSetUp() {
        if (UserLocation.getLastLocation() != null) {
            updateCurrentPositionCamera(UserLocation.getLatitude(), UserLocation.getLongitude());
        } else {
            getLastLocation();
        }
        this.setUp1KmRadius();

    }

    private void setUpMap() {
        currentPositionSetUp();
        this.map.getUiSettings().setMapToolbarEnabled(false);
        this.map.setOnMarkerClickListener(this);
        this.map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.map.setOnMarkerDragListener(this);
        this.map.setOnCameraIdleListener(this);
        this.map.setOnCameraMoveStartedListener(this);
        this.map.setOnCameraMoveListener(this);
        this.map.setOnCameraMoveCanceledListener(this);

//        this.map.setMaxZoomPreference(14);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        M.l("Google Map is ready");
        this.map = googleMap;
        setUpMap();

    }

    void setCirclePosition(LatLng latLng) {
        this.circle.setCenter(latLng);
    }

    private void updateCurrentPositionCamera(double mLat, double mLongi) {
        currentCameraPosition = new CameraPosition.Builder().target(new LatLng(mLat, mLongi))
                .zoom(15.0f)
                .bearing(30)
                .tilt(35)
                .build();
        this.map.animateCamera(CameraUpdateFactory.newCameraPosition(currentCameraPosition));


    }

    public void DisplayPlaceOnMap(Double lat, Double longi, String name, String PlaceID, boolean animate) {
        Marker marker = this.map.addMarker(new MarkerOptions().position(new LatLng(lat, longi)).title(name));
        marker.setTag(PlaceID);
        marker.setIcon(BitmapDescriptorFactory.fromResource(this.mainActivity.getIconforMap()));
        if (animate)
            this.map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, longi), 14));

    }


    public void DisplayNearBy(nearby nb) {
        placesOnMap = true;
        if (!NextPageToken)
            this.map.clear();
        this.setUp1KmRadius();
        this.setCirclePosition(new LatLng(UserLocation.getLatitude(), UserLocation.getLongitude()));
        List<Result> Placeslist = nb.getResults();
        double lat, lng;
        for (int i = 0; i < Placeslist.size(); i++) {
            lat = Placeslist.get(i).getGeometry().getLocation().getLat();
            lng = Placeslist.get(i).getGeometry().getLocation().getLng();
            DisplayPlaceOnMap(lat, lng, Placeslist.get(i).getName(), Placeslist.get(i).getPlaceId(), false);
        }
    }

    void clearMap() {
        placesOnMap = false;
        this.map.clear();

    }


    private void getLastLocation() {
        M.l(" fragment getting last current");
        ((MainActivity) getActivity()).getLocation(this);
    }


    @Override
    public void PlacesResponse(String Places) {
        M.l(Places);
        this.directionset = false;
        if (Places != null) {
            Gson gson = new Gson();
            nb = gson.fromJson(Places, nearby.class);
            M.l(nb.getStatus());
            if (nb.getStatus().equals("OK")) {
                DisplayNearBy(nb);
                if (nb.getNextPageToken() != null) {
                    NextPageToken = true;
                    M.l("get next page places");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mainActivity.NextPagePlaces(nb.getNextPageToken());
                        }
                    }, 1500);


                } else NextPageToken = false;


            } else if (nb.getStatus().equals("ZERO_RESULTS")) {
                M.t(context, "No Result found at this place");
            } else M.t(context, "Failed to get places");
        }


    }

    @Override
    public void setPlaceOnMap(Double lat, Double lng, String name, String PlaceID) {
        this.directionset = false;
        DisplayPlaceOnMap(lat, lng, name, PlaceID, true);
    }

    @Override
    public void showIndicator() {
        this.loadingIndicatorView.smoothToShow();
    }

    @Override
    public void hideIndicator() {
        this.loadingIndicatorView.smoothToHide();

    }

    @Override
    public void DrawDirection(Direction direction) {
        this.map.addMarker(new MarkerOptions().position(new LatLng(UserLocation.getUserActualLocation().getLatitude(), UserLocation.getLongitude())).title(direction.getRoutes().get(0).getLegs().get(0).getStartAddress()));
        this.map.addMarker(new MarkerOptions().position(new LatLng(direction.getRoutes().get(0).getLegs().get(0).getEndLocation().getLat(),direction.getRoutes().get(0).getLegs().get(0).getEndLocation().getLng())).title(direction.getRoutes().get(0).getLegs().get(0).getEndAddress()));
        this.map.animateCamera(CameraUpdateFactory.newLatLngBounds(
                new LatLngBounds(
                        new LatLng(UserLocation.getLatitude(),UserLocation.getLongitude())
                        ,new LatLng(direction.getRoutes().get(0).getLegs().get(0).getEndLocation().getLat()
                                    ,direction.getRoutes().get(0).getLegs().get(0).getEndLocation().getLng()
                                    )
                            ),150));
        PolylineOptions polylineOptions = new PolylineOptions().
                geodesic(true).
                color(Color.parseColor("#00a7aa")).
                width(10);
        List<LatLng> way;
        way = decodePolyLine(direction.getRoutes().get(0).getOverviewPolyline().getPoints());
        // this.map.addMarker(new MarkerOptions().position(new LatLng(way.latitude,way.longitude)).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon)));

        M.l("DrawDirection");
        for (int i = 0; i < way.size(); i++)
            polylineOptions.add(way.get(i));

        this.map.addPolyline(polylineOptions);
        this.directionset = true;

    }

    @Override
    public void onComplete(@NonNull Task<Location> task) {
        if (task.isSuccessful()) {
            UserLocation.setLastLocation(task.getResult());
            if (UserLocation.getLastLocation() != null) {
                UserLocation.setUserActualLocation();
                updateCurrentPositionCamera(UserLocation.getLatitude(), UserLocation.getLongitude());
                this.setCirclePosition(new LatLng(UserLocation.getLatitude(), UserLocation.getLongitude()));

            } else
                M.l("my current is null");
        } else
            M.l("Cant get Location");
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        if (marker.getTag() != null) {
            M.l(marker.getId());
            M.l(marker.getPosition().latitude + " " + marker.getPosition().longitude);
            M.l(marker.getTag().toString());
            this.mainActivity.getPlaceDetail(Utils.getPlaceDetailRequestUrl(marker.getTag().toString()), Constant.PLACE_DETAIL);
        }
        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

        //this.userCurrentLocation=marker;
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        //this.userCurrentLocation=marker;

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
//        this.updateCurrentPositionCamera(marker.getPosition().latitude,marker.getPosition().longitude,false);
//        this.mainActivity.setUserCurrentLocation(marker.getPosition(),getPlcaes);
//        mLastLocation.setLongitude(marker.getPosition().longitude);
//        mLastLocation.setLatitude(marker.getPosition().latitude);

        //this.userCurrentLocation=marker;
    }

    @Override
    public void onCameraIdle() {
        M.l("onCameraIdle");
        if (!directionset) {
            newCurrent.setLatitude(this.map.getCameraPosition().target.latitude);
            newCurrent.setLongitude(this.map.getCameraPosition().target.longitude);
            if (UserLocation.getLastLocation() != null) {
                M.l("new current " + UserLocation.getLastLocation().distanceTo(newCurrent));
                this.distance = (int) UserLocation.getLastLocation().distanceTo(newCurrent);
                if (this.distance > Constant.Aproximity_Radious) {
                    M.l("distance is more then 1000");
                    this.getPlcaes = true;

                } else {
                    M.l("distance is less then 1000");
                    this.getPlcaes = false;
                }
                if ((getPlcaes && placesOnMap) || !placesOnMap) {
                    UserLocation.setLastLocation(this.map.getCameraPosition().target.latitude, this.map.getCameraPosition().target.longitude);
                    this.setCirclePosition(new LatLng(UserLocation.getLatitude(), UserLocation.getLongitude()));
                }

                this.mainActivity.getPlaces(getPlcaes);
            }
        }
    }

    @Override
    public void onCameraMoveCanceled() {
        M.l("onCameraMoveCanceled");

    }

    @Override
    public void onCameraMove() {
        M.l("onCameraMove");

    }

    @Override
    public void onCameraMoveStarted(int i) {
        M.l("onCameraMoveStarted");


    }

    private List<LatLng> decodePolyLine(final String poly) {
        int len = poly.length();
        int index = 0;
        List<LatLng> decoded = new ArrayList<LatLng>();
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int b;
            int shift = 0;
            int result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;
            M.l("lat lng after decoding:" + lat + " " + lng);
            decoded.add(new LatLng(
                    lat / 100000d, lng / 100000d
            ));
        }

        return decoded;
    }

}
